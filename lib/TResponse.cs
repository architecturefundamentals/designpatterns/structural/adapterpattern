using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Adapter.lib
{
    public class TResponse
    {
        public Boolean Success {
            get;
            set;
        }
        public String ErrorMessage {
            get;
            set;
        }

        public TResponse(Boolean success, String errorMessage){
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

    }
}