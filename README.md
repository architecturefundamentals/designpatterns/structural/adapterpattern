# ADAPTER METHOD PATTERN

---
## A bit of context ...
>### _What is this pattern about?_
>This pattern it's a structural pattern, it allows objects with incompatible interfaces to collaborate.
>
> It's usually used in the next cases:
>  * When you want to use some existing class, but its interface isn’t compatible with the rest of your cod
>  * When you want to reuse several existing subclasses that lack some common functionality that can’t be added to the superclass.
> 
---
## But this simple example is about...

![Demo](img/Adapter Pattern-Adapter.jpg)

and Done!
:smile:
---
_Tech: C#_ 