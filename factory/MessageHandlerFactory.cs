using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Adapter.factory
{
    public class MessageHandlerFactory : MessagerCreator
    {
        public override IMessager create()
        {
            return new MessageHandler();
        }
    }
}