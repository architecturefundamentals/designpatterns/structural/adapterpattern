using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Adapter.lib
{
    public class TMessage
    {
        private String Name{
            get;
            set;
        }
        public String Message{
            get;
            set;
        }

        public TMessage(String name, String message){
            this.Name = name;
            this.Message = message;
        }

    }
}