﻿using FactoryMethod.Adapter;
using FactoryMethod.Adapter.factory;


#region Adapter

IMessager messager = new MessageHandler();

messager.sendMessage("Holi!...");

TelegramMessageHandler telegramMessageHandler = new TelegramMessageHandler();
IMessager messager02 = new TelegramMessageAdapter(telegramMessageHandler);

messager02.sendMessage("Bye! ..");

//FACTORY METHOD
IMessager messager03 = new MessageHandlerFactory().create();
messager03.sendMessage("Hi Adapter!");

IMessager messager04 = new TelegramMessageFactory().create();
messager04.sendMessage("Bye Adapter! ");

#endregion








