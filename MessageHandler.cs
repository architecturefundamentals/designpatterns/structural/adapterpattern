namespace FactoryMethod.Adapter
{
    public class MessageHandler : IMessager
    {
        public bool sendMessage(string message)
        {
            Console.WriteLine("Mensaje: " + message);
            return true;
        }
    }
}